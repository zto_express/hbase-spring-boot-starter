package com.zto.boot.hbase.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>Class: HColumn</p>
 * <p>Description: HBase 字段属性注解 标记一个属性别名</p>
 * @author xiaowenke@zto.cn
 * @date 2017-11-21
 * @version 1.0
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface HColumn {
    String name();

    String family() default "info";
}
