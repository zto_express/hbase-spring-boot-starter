package com.zto.boot.hbase.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * <p>Class: HTable </p>
 * <p>Description: HBase 实体注解 标记一个实体为Table</p>
 * @author xiaowenke@zto.cn
 * @date 2017-11-21
 * @version 1.0
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface HTable {
     String name();
}
