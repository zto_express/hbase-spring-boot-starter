/**
 * <p>Title: HBaseConfigProperties.java</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: www.zto.com</p>
 */
package com.zto.boot.hbase.configuration;

import com.zto.boot.hbase.consts.Const;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>Class: HBaseConfigProperties</p>
 * <p>Description: </p>
 *
 * @author xiaowenke@zto.cn
 * @version 1.0
 * @date 2018/5/19
 */
@ConfigurationProperties(prefix = Const.HBASE_PREFIX)
public class HBaseConfigProperties {



    @Value("${hbase.client.retries.number:10}")
    private Integer clientRetriesNumber;
    @Value("${hbase.increments.buffer_size:65535}")
    private Integer incrementsBufferSize;
    @Value("${hbase.increments.durable:true}")
    private Boolean incrementsDurable;
    @Value("${hbase.ipc.client.connection.idle_timeout:300}")
    private Integer ipcClientConnectionIdleTimeout;
    @Value("${hbase.ipc.client.socket.receiveBufferSize:}")
    private Integer ipcClientSocketReceiveBufferSize;
    @Value("${hbase.ipc.client.socket.sendBufferSize:}")
    private Integer ipcClientSocketSendBufferSize;
    @Value("${hbase.ipc.client.socket.timeout.connect:5000}")
    private Integer ipcClientSocketTimeoutConnect;
    @Value("${hbase.ipc.client.tcpkeepalive:true}")
    private Boolean ipcClientTcpkeepalive;
    @Value("${hbase.ipc.client.tcpnodelay:true}")
    private Boolean ipcClientTcpnodelay;
    @Value("${hbase.kerberos.regionserver.principal:}")
    private String kerberosRegionserverPrincipal;
    @Value("${hbase.nsre.high_watermark:10000}")
    private Integer nsreHighWatermark;
    @Value("${hbase.nsre.low_watermark:1000}")
    private Integer nsreLowWatermark;
    @Value("${hbase.region_client.check_channel_write_status:false}")
    private Boolean regionClientCheckChannelWriteStatus;
    @Value("${hbase.region_client.inflight_limit:0}")
    private Integer regionClientInflightLimit;
    @Value("${hbase.region_client.pending_limit:0}")
    private Integer regionClientPendingLimit;
    @Value("${hbase.regionserver.kerberos.password:}")
    private String regionserverKerberosPassword;
    @Value("${hbase.rpcs.batch.size:1024}")
    private Integer rpcsBatchSize;
    @Value("${hbase.rpcs.buffered_flush_interval:1000}")
    private Integer rpcsBufferedFlushInterval;
    @Value("${hbase.rpc.protection:}")
    private String rpcProtection;
    @Value("${hbase.rpc.timeout:0}")
    private Integer rpcTimeout;
    @Value("${hbase.sasl.clientconfig:Client}")
    private String saslClientconfig;
    @Value("${hbase.security.auth.94:false}")
    private Boolean securityAuth94;
    @Value("${hbase.security.auth.enable:}")
    private Boolean securityAuthEnable;
    @Value("${hbase.security.authentication:}")
    private String securityAuthentication;
    @Value("${hbase.security.simple.username:}")
    private String securitySimpleUsername;
    @Value("${hbase.timer.tick:20}")
    private Integer timerTick;
    @Value("${hbase.timer.ticks_per_wheel:512}")
    private Integer timerTicksPerWheel;
    @Value("${hbase.workers.size:}")
    private Integer workersSize;
    @Value("${hbase.zookeeper.getroot.retry_delay:1000}")
    private Integer zookeeperGetrootRetryDelay;
    @Value("${hbase.zookeeper.quorum}")
    private String zookeeperQuorum;
    @Value("${hbase.zookeeper.session.timeout:5000}")
    private Integer zookeeperSessionTimeout;
    @Value("${hbase.zookeeper.znode.parent:/hbase}")
    private String zookeeperZnodeParent;

    public Boolean getIncrementsDurable() {
        return incrementsDurable;
    }

    public void setIncrementsDurable(Boolean incrementsDurable) {
        this.incrementsDurable = incrementsDurable;
    }

    public Integer getIpcClientConnectionIdleTimeout() {
        return ipcClientConnectionIdleTimeout;
    }

    public void setIpcClientConnectionIdleTimeout(Integer ipcClientConnectionIdleTimeout) {
        this.ipcClientConnectionIdleTimeout = ipcClientConnectionIdleTimeout;
    }

    public Integer getIpcClientSocketReceiveBufferSize() {
        return ipcClientSocketReceiveBufferSize;
    }

    public void setIpcClientSocketReceiveBufferSize(Integer ipcClientSocketReceiveBufferSize) {
        this.ipcClientSocketReceiveBufferSize = ipcClientSocketReceiveBufferSize;
    }

    public Integer getIpcClientSocketSendBufferSize() {
        return ipcClientSocketSendBufferSize;
    }

    public void setIpcClientSocketSendBufferSize(Integer ipcClientSocketSendBufferSize) {
        this.ipcClientSocketSendBufferSize = ipcClientSocketSendBufferSize;
    }

    public Integer getIpcClientSocketTimeoutConnect() {
        return ipcClientSocketTimeoutConnect;
    }

    public void setIpcClientSocketTimeoutConnect(Integer ipcClientSocketTimeoutConnect) {
        this.ipcClientSocketTimeoutConnect = ipcClientSocketTimeoutConnect;
    }

    public Boolean getIpcClientTcpkeepalive() {
        return ipcClientTcpkeepalive;
    }

    public void setIpcClientTcpkeepalive(Boolean ipcClientTcpkeepalive) {
        this.ipcClientTcpkeepalive = ipcClientTcpkeepalive;
    }

    public Boolean getIpcClientTcpnodelay() {
        return ipcClientTcpnodelay;
    }

    public void setIpcClientTcpnodelay(Boolean ipcClientTcpnodelay) {
        this.ipcClientTcpnodelay = ipcClientTcpnodelay;
    }

    public String getKerberosRegionserverPrincipal() {
        return kerberosRegionserverPrincipal;
    }

    public void setKerberosRegionserverPrincipal(String kerberosRegionserverPrincipal) {
        this.kerberosRegionserverPrincipal = kerberosRegionserverPrincipal;
    }

    public Integer getNsreHighWatermark() {
        return nsreHighWatermark;
    }

    public void setNsreHighWatermark(Integer nsreHighWatermark) {
        this.nsreHighWatermark = nsreHighWatermark;
    }

    public Integer getNsreLowWatermark() {
        return nsreLowWatermark;
    }

    public void setNsreLowWatermark(Integer nsreLowWatermark) {
        this.nsreLowWatermark = nsreLowWatermark;
    }

    public Boolean getRegionClientCheckChannelWriteStatus() {
        return regionClientCheckChannelWriteStatus;
    }

    public void setRegionClientCheckChannelWriteStatus(Boolean regionClientCheckChannelWriteStatus) {
        this.regionClientCheckChannelWriteStatus = regionClientCheckChannelWriteStatus;
    }

    public Integer getRegionClientInflightLimit() {
        return regionClientInflightLimit;
    }

    public void setRegionClientInflightLimit(Integer regionClientInflightLimit) {
        this.regionClientInflightLimit = regionClientInflightLimit;
    }

    public Integer getRegionClientPendingLimit() {
        return regionClientPendingLimit;
    }

    public void setRegionClientPendingLimit(Integer regionClientPendingLimit) {
        this.regionClientPendingLimit = regionClientPendingLimit;
    }

    public String getRegionserverKerberosPassword() {
        return regionserverKerberosPassword;
    }

    public void setRegionserverKerberosPassword(String regionserverKerberosPassword) {
        this.regionserverKerberosPassword = regionserverKerberosPassword;
    }

    public Integer getRpcsBatchSize() {
        return rpcsBatchSize;
    }

    public void setRpcsBatchSize(Integer rpcsBatchSize) {
        this.rpcsBatchSize = rpcsBatchSize;
    }

    public Integer getRpcsBufferedFlushInterval() {
        return rpcsBufferedFlushInterval;
    }

    public void setRpcsBufferedFlushInterval(Integer rpcsBufferedFlushInterval) {
        this.rpcsBufferedFlushInterval = rpcsBufferedFlushInterval;
    }

    public String getRpcProtection() {
        return rpcProtection;
    }

    public void setRpcProtection(String rpcProtection) {
        this.rpcProtection = rpcProtection;
    }

    public Integer getRpcTimeout() {
        return rpcTimeout;
    }

    public void setRpcTimeout(Integer rpcTimeout) {
        this.rpcTimeout = rpcTimeout;
    }

    public String getSaslClientconfig() {
        return saslClientconfig;
    }

    public void setSaslClientconfig(String saslClientconfig) {
        this.saslClientconfig = saslClientconfig;
    }

    public Boolean getSecurityAuth94() {
        return securityAuth94;
    }

    public void setSecurityAuth94(Boolean securityAuth94) {
        this.securityAuth94 = securityAuth94;
    }

    public Boolean getSecurityAuthEnable() {
        return securityAuthEnable;
    }

    public void setSecurityAuthEnable(Boolean securityAuthEnable) {
        this.securityAuthEnable = securityAuthEnable;
    }

    public String getSecurityAuthentication() {
        return securityAuthentication;
    }

    public void setSecurityAuthentication(String securityAuthentication) {
        this.securityAuthentication = securityAuthentication;
    }

    public String getSecuritySimpleUsername() {
        return securitySimpleUsername;
    }

    public void setSecuritySimpleUsername(String securitySimpleUsername) {
        this.securitySimpleUsername = securitySimpleUsername;
    }

    public Integer getTimerTick() {
        return timerTick;
    }

    public void setTimerTick(Integer timerTick) {
        this.timerTick = timerTick;
    }

    public Integer getTimerTicksPerWheel() {
        return timerTicksPerWheel;
    }

    public void setTimerTicksPerWheel(Integer timerTicksPerWheel) {
        this.timerTicksPerWheel = timerTicksPerWheel;
    }

    public Integer getWorkersSize() {
        return workersSize;
    }

    public void setWorkersSize(Integer workersSize) {
        this.workersSize = workersSize;
    }

    public Integer getZookeeperGetrootRetryDelay() {
        return zookeeperGetrootRetryDelay;
    }

    public void setZookeeperGetrootRetryDelay(Integer zookeeperGetrootRetryDelay) {
        this.zookeeperGetrootRetryDelay = zookeeperGetrootRetryDelay;
    }

    public String getZookeeperQuorum() {
        return zookeeperQuorum;
    }

    public void setZookeeperQuorum(String zookeeperQuorum) {
        this.zookeeperQuorum = zookeeperQuorum;
    }

    public Integer getZookeeperSessionTimeout() {
        return zookeeperSessionTimeout;
    }

    public void setZookeeperSessionTimeout(Integer zookeeperSessionTimeout) {
        this.zookeeperSessionTimeout = zookeeperSessionTimeout;
    }

    public String getZookeeperZnodeParent() {
        return zookeeperZnodeParent;
    }

    public void setZookeeperZnodeParent(String zookeeperZnodeParent) {
        this.zookeeperZnodeParent = zookeeperZnodeParent;
    }

    public Integer getClientRetriesNumber() {
        return clientRetriesNumber;
    }

    public void setClientRetriesNumber(Integer clientRetriesNumber) {
        this.clientRetriesNumber = clientRetriesNumber;
    }

    public Integer getIncrementsBufferSize() {
        return incrementsBufferSize;
    }

    public void setIncrementsBufferSize(Integer incrementsBufferSize) {
        this.incrementsBufferSize = incrementsBufferSize;
    }
}
