/**
 * <p>Title: HbaseConfiguration.java</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: www.zto.com</p>
 */
package com.zto.boot.hbase.configuration;

import com.zto.boot.hbase.HBaseTemplate;
import org.hbase.async.Config;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.zto.boot.hbase.consts.Const.*;

/**
 * <p>Class: HbaseConfiguration</p>
 * <p>Description: </p>
 *
 * @author xiaowenke@zto.cn
 * @version 1.0
 * @date 2018/5/19
 */
@Configuration
@EnableConfigurationProperties({HBaseConfigProperties.class})
@ConditionalOnProperty(prefix = "hbase.zookeeper", name = "quorum", matchIfMissing = true)
public class HBaseConfiguration {


    @Bean
    public HBaseTemplate hBaseTemplate(HBaseConfigProperties properties) {
        Config config = new Config();

        config.overrideConfig(HBASE_CLIENT_RETRIES_NUMBER, properties.getClientRetriesNumber() + "");
        config.overrideConfig(HBASE_INCREMENTS_BUFFER_SIZE, properties.getIncrementsBufferSize() + "");
        config.overrideConfig(HBASE_INCREMENTS_DURABLE, properties.getIncrementsDurable() + "");
        config.overrideConfig(HBASE_IPC_CLIENT_CONNECTION_IDLE_TIMEOUT, properties.getIpcClientConnectionIdleTimeout() + "");
        if (properties.getIpcClientSocketReceiveBufferSize() != null) {
            config.overrideConfig(HBASE_IPC_CLIENT_SOCKET_RECEIVEBUFFERSIZE, properties.getIpcClientSocketReceiveBufferSize() + "");
        }
        if (properties.getIpcClientSocketSendBufferSize() != null) {
            config.overrideConfig(HBASE_IPC_CLIENT_SOCKET_SENDBUFFERSIZE, properties.getIpcClientSocketSendBufferSize() + "");
        }
        config.overrideConfig(HBASE_IPC_CLIENT_SOCKET_TIMEOUT_CONNECT, properties.getIpcClientSocketTimeoutConnect() + "");
        config.overrideConfig(HBASE_IPC_CLIENT_TCPKEEPALIVE, properties.getIpcClientTcpkeepalive() + "");
        config.overrideConfig(HBASE_IPC_CLIENT_TCPNODELAY, properties.getIpcClientTcpnodelay() + "");
        if (properties.getKerberosRegionserverPrincipal() != null) {
            config.overrideConfig(HBASE_KERBEROS_REGIONSERVER_PRINCIPAL, properties.getKerberosRegionserverPrincipal());
        }
        config.overrideConfig(HBASE_NSRE_HIGH_WATERMARK, properties.getNsreHighWatermark() + "");
        config.overrideConfig(HBASE_NSRE_LOW_WATERMARK, properties.getNsreLowWatermark() + "");
        config.overrideConfig(HBASE_REGION_CLIENT_CHECK_CHANNEL_WRITE_STATUS, properties.getRegionClientCheckChannelWriteStatus() + "");
        config.overrideConfig(HBASE_REGION_CLIENT_INFLIGHT_LIMIT, properties.getRegionClientInflightLimit() + "");
        config.overrideConfig(HBASE_REGION_CLIENT_PENDING_LIMIT, properties.getRegionClientPendingLimit() + "");
        if (properties.getRegionserverKerberosPassword() != null) {
            config.overrideConfig(HBASE_REGIONSERVER_KERBEROS_PASSWORD, properties.getRegionserverKerberosPassword());
        }
        config.overrideConfig(HBASE_RPCS_BATCH_SIZE, properties.getRpcsBatchSize() + "");
        config.overrideConfig(HBASE_RPCS_BUFFERED_FLUSH_INTERVAL, properties.getRpcsBufferedFlushInterval() + "");
        if (properties.getRpcProtection() != null) {
            config.overrideConfig(HBASE_RPC_PROTECTION, properties.getRpcProtection());
        }
        config.overrideConfig(HBASE_RPC_TIMEOUT, properties.getRpcTimeout() + "");
        config.overrideConfig(HBASE_SASL_CLIENTCONFIG, properties.getSaslClientconfig());
        config.overrideConfig(HBASE_SECURITY_AUTH_94, properties.getSecurityAuth94() + "");
        if (properties.getSecurityAuthEnable() != null) {
            config.overrideConfig(HBASE_SECURITY_AUTH_ENABLE, properties.getSecurityAuthEnable() + "");
        }
        if (properties.getSecurityAuthentication() != null) {
            config.overrideConfig(HBASE_SECURITY_AUTHENTICATION, properties.getSecurityAuthentication());
        }
        if (properties.getSecuritySimpleUsername() != null) {
            config.overrideConfig(HBASE_SECURITY_SIMPLE_USERNAME, properties.getSecuritySimpleUsername());
        }
        config.overrideConfig(HBASE_TIMER_TICK, properties.getTimerTick() + "");
        config.overrideConfig(HBASE_TIMER_TICKS_PER_WHEEL, properties.getTimerTicksPerWheel() + "");
        if (properties.getWorkersSize() != null) {
            config.overrideConfig(HBASE_WORKERS_SIZE, properties.getWorkersSize() + "");
        }
        config.overrideConfig(HBASE_ZOOKEEPER_GETROOT_RETRY_DELAY, properties.getZookeeperGetrootRetryDelay() + "");
        if (properties.getZookeeperQuorum() != null) {
            config.overrideConfig(HBASE_ZOOKEEPER_QUORUM, properties.getZookeeperQuorum());
        }
        config.overrideConfig(HBASE_ZOOKEEPER_SESSION_TIMEOUT, properties.getZookeeperSessionTimeout() + "");
        config.overrideConfig(HBASE_ZOOKEEPER_ZNODE_PARENT, properties.getZookeeperZnodeParent());

        return new HBaseTemplate(config);
    }


}
