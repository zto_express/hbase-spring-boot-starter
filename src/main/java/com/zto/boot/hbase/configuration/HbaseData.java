package com.zto.boot.hbase.configuration;

import java.io.Serializable;
import java.util.Arrays;

public class HbaseData implements Serializable {

    private byte[] family;

    private byte[] qualifier;

    private byte[] value;


    public HbaseData(byte[] family, byte[] qualifier, byte[] value) {
        this.family = family;
        this.qualifier = qualifier;
        this.value = value;
    }

    public byte[] getFamily() {
        return family;
    }

    public void setFamily(byte[] family) {
        this.family = family;
    }

    public byte[] getQualifier() {
        return qualifier;
    }

    public void setQualifier(byte[] qualifier) {
        this.qualifier = qualifier;
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HbaseData data = (HbaseData) o;

        if (!Arrays.equals(family, data.family)) {
            return false;
        }
        if (!Arrays.equals(qualifier, data.qualifier)) {
            return false;
        }
        return Arrays.equals(value, data.value);
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(family);
        result = 31 * result + Arrays.hashCode(qualifier);
        result = 31 * result + Arrays.hashCode(value);
        return result;
    }
}
