/**
 * <p>Title: Const.java</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: www.zto.com</p>
 */
package com.zto.boot.hbase.consts;

/**
 * <p>Class: Const</p>
 * <p>Description: </p>
 *
 * @author xiaowenke@zto.cn
 * @version 1.0
 * @date 2018/5/19
 */
public class Const {

    public static final String HBASE_PREFIX = "hbase";
    public static final String HBASE_CLIENT_RETRIES_NUMBER = "hbase.client.retries.number";
    public static final String HBASE_INCREMENTS_BUFFER_SIZE = "hbase.increments.buffer_size";
    public static final String HBASE_INCREMENTS_DURABLE = "hbase.increments.durable";
    public static final String HBASE_IPC_CLIENT_CONNECTION_IDLE_TIMEOUT = "hbase.ipc.client.connection.idle_timeout";
    public static final String HBASE_IPC_CLIENT_SOCKET_RECEIVEBUFFERSIZE = "hbase.ipc.client.socket.receiveBufferSize";
    public static final String HBASE_IPC_CLIENT_SOCKET_SENDBUFFERSIZE = "hbase.ipc.client.socket.sendBufferSize";
    public static final String HBASE_IPC_CLIENT_SOCKET_TIMEOUT_CONNECT = "hbase.ipc.client.socket.timeout.connect";
    public static final String HBASE_IPC_CLIENT_TCPKEEPALIVE = "hbase.ipc.client.tcpkeepalive";
    public static final String HBASE_IPC_CLIENT_TCPNODELAY = "hbase.ipc.client.tcpnodelay";
    public static final String HBASE_KERBEROS_REGIONSERVER_PRINCIPAL = "hbase.kerberos.regionserver.principal";
    public static final String HBASE_NSRE_HIGH_WATERMARK = "hbase.nsre.high_watermark";
    public static final String HBASE_NSRE_LOW_WATERMARK = "hbase.nsre.low_watermark";
    public static final String HBASE_REGION_CLIENT_CHECK_CHANNEL_WRITE_STATUS = "hbase.region_client.check_channel_write_status";
    public static final String HBASE_REGION_CLIENT_INFLIGHT_LIMIT = "hbase.region_client.inflight_limit";
    public static final String HBASE_REGION_CLIENT_PENDING_LIMIT = "hbase.region_client.pending_limit";
    public static final String HBASE_REGIONSERVER_KERBEROS_PASSWORD = "hbase.regionserver.kerberos.password";
    public static final String HBASE_RPCS_BATCH_SIZE = "hbase.rpcs.batch.size";
    public static final String HBASE_RPCS_BUFFERED_FLUSH_INTERVAL = "hbase.rpcs.buffered_flush_interval";
    public static final String HBASE_RPC_PROTECTION = "hbase.rpc.protection";
    public static final String HBASE_RPC_TIMEOUT = "hbase.rpc.timeout";
    public static final String HBASE_SASL_CLIENTCONFIG = "hbase.sasl.clientconfig";
    public static final String HBASE_SECURITY_AUTH_94 = "hbase.security.auth.94";
    public static final String HBASE_SECURITY_AUTH_ENABLE = "hbase.security.auth.enable";
    public static final String HBASE_SECURITY_AUTHENTICATION = "hbase.security.authentication";
    public static final String HBASE_SECURITY_SIMPLE_USERNAME = "hbase.security.simple.username";
    public static final String HBASE_TIMER_TICK = "hbase.timer.tick";
    public static final String HBASE_TIMER_TICKS_PER_WHEEL = "hbase.timer.ticks_per_wheel";
    public static final String HBASE_WORKERS_SIZE = "hbase.workers.size";
    public static final String HBASE_ZOOKEEPER_GETROOT_RETRY_DELAY = "hbase.zookeeper.getroot.retry_delay";
    public static final String HBASE_ZOOKEEPER_QUORUM = "hbase.zookeeper.quorum";
    public static final String HBASE_ZOOKEEPER_SESSION_TIMEOUT = "hbase.zookeeper.session.timeout";
    public static final String HBASE_ZOOKEEPER_ZNODE_PARENT = "hbase.zookeeper.znode.parent";

}
