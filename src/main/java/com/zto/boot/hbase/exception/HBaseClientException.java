/**
 * <p>Title: HBaseClientException.java</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: www.zto.com</p>
 */
package com.zto.boot.hbase.exception;

/**
 * <p>Class: HBaseClientException</p>
 * <p>Description: </p>
 * @author xiaowenke@zto.cn
 * @date 2018/5/19
 * @version 1.0
 */
public class HBaseClientException extends RuntimeException {
    private static final long serialVersionUID = 4013267759338628736L;

    public static final String ERR_DEF="DEF001";

    private String code;

    public HBaseClientException() {
    }
    /**
     * 传入code 和Msg 构建HBaseClientException对象
     * @param code
     * @param message
     */
    public HBaseClientException(String code, String message) {
        super(message);
        this.code = code;
    }

    public HBaseClientException(String message) {
        super(message);
    }

    public HBaseClientException(Throwable cause) {
        super(cause);
    }

    public HBaseClientException(String message, Throwable cause) {
        super(message, cause);
    }

    /***
     * 提供一个简便的构建HBaseClientException对象方法
     * @param patterm  异常信息模板 如 xxxx {} xxxx
     * @param args  传入模板所需要的参数
     * @return
     */
    public static HBaseClientException exception(String patterm, Object... args) {
        String msg = java.text.MessageFormat.format(patterm,args);
        return new HBaseClientException(ERR_DEF,msg);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
