package com.zto.boot.hbase.function;

import java.util.function.BiFunction;

public interface DefaultFunction<R> extends BiFunction<Object, Exception, R> {

    @Override
    R apply(Object o, Exception e) ;

}
