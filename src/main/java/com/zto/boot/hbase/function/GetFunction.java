package com.zto.boot.hbase.function;

import java.util.function.BiFunction;

public interface GetFunction<T,R> extends BiFunction<T, Exception, R> {

    @Override
    R apply(T t, Exception e);
}
