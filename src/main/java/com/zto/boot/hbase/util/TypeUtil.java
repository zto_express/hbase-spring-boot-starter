package com.zto.boot.hbase.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TypeUtil {

    public static boolean isLongType(Field f){
        if (f == null) {
            return false;
        }
        return "long".equals(f.getType().getName()) || f.getType().isAssignableFrom(Long.class);
    }

    public static boolean isShortType(Field f){
        if (f == null) {
            return false;
        }
        return "short".equals(f.getType().getName()) || f.getType().isAssignableFrom(Short.class);
    }

    public static boolean isIntType(Field f){
        if (f == null) {
            return false;
        }
        return "int".equals(f.getType().getName()) || f.getType().isAssignableFrom(Integer.class);
    }

    public static boolean isFloatType(Field f){
        if (f == null) {
            return false;
        }
        return "float".equals(f.getType().getName()) || f.getType().isAssignableFrom(Float.class);
    }

    public static boolean isDoubleType(Field f){
        if (f == null) return false;
        return "double".equals(f.getType().getName()) || f.getType().isAssignableFrom(Double.class);
    }

    public static boolean isBooleanType(Field f){
        if (f == null) return false;
        return "boolean".equals(f.getType().getName()) || f.getType().isAssignableFrom(Boolean.class);
    }

    public static boolean isBigDecimalType(Field f){
        if (f == null) return false;
        return f.getType().isAssignableFrom(BigDecimal.class);
    }

    public static boolean isStringType(Field f){
        if (f == null) return false;
        return f.getType().isAssignableFrom(String.class);
    }

    public static String getSuitableTypes(){
        List<String> result = new ArrayList<String>();
        result.add("boolean[Boolean]");
        result.add("short[Short]");
        result.add("int[Integer]");
        result.add("long[Long]");
        result.add("float[Float]");
        result.add("double[Double]");
        result.add("String");
        result.add("BigDecimal");
        result.add("Date");
        return join(result.toArray(), ',');
    }


    public static String join(Object[] array, char separator) {
        return array == null ? null : join(array, separator, 0, array.length);
    }

    public static String join(Object[] array, char separator, int startIndex, int endIndex) {
        if (array == null) {
            return null;
        } else {
            int noOfItems = endIndex - startIndex;
            if (noOfItems <= 0) {
                return "";
            } else {
                StringBuilder buf = new StringBuilder(noOfItems * 16);

                for(int i = startIndex; i < endIndex; ++i) {
                    if (i > startIndex) {
                        buf.append(separator);
                    }

                    if (array[i] != null) {
                        buf.append(array[i]);
                    }
                }

                return buf.toString();
            }
        }
    }
}
