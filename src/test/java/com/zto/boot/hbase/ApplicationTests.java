package com.zto.boot.hbase;

import com.alibaba.fastjson.JSON;
import com.zto.boot.hbase.annotation.*;
import com.zto.boot.hbase.configuration.HBaseConfigProperties;
import com.zto.boot.hbase.model.Student;
import org.hbase.async.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ApplicationTests {

	@Autowired
	private HBaseConfigProperties configProperties;
	@Test
	public void contextLoads() {
		System.out.println(configProperties.getClientRetriesNumber()+","+configProperties.getIncrementsBufferSize());
		System.out.println(configProperties.getZookeeperQuorum());
	}

	private static final String TABLE_NAME = "SEA_TEST";

	private static final String FAMILY = "info";

	@Autowired
	private HBaseTemplate hBaseTemplate;

	@Test
	public void testDelete() throws InterruptedException {
		List list = new ArrayList();
		hBaseTemplate.delete( Student.class, (o,e) -> {
			if (o != null) {
				System.out.println("==== o ======"+o);
				list.add(o);
				return o;

			}
			if (e != null) {
				System.out.println("=================================");
				System.out.println(e.getClass());
				System.out.println("=================================");
				e.printStackTrace();
				list.add(e);
			}
			return null;
		}, "李3","李4");

		while (list.size()<=0){
			Thread.sleep(1000L);
		}
		Object o = list.get(0);
		if(o instanceof Exception){
			Assert.assertTrue(false);
		}else{
			Assert.assertTrue(true);
		}

	}

	@Test
	public void testGet() throws InterruptedException {

		String prefix = "李";
		for (int i = 5; i <= 8; i++) {
			String rowKey = prefix + i;
			System.out.println(rowKey);
			System.out.println(hBaseTemplate.get(Student.class, rowKey, (student1, e) -> {

				if (student1 != null) {
						System.out.println("=============================");
						System.out.println(JSON.toJSONString(student1));
						System.out.println("=============================");
					return student1;
				}

				if (e!=null) {
					e.printStackTrace();
				}
				return null;
			}));
		}

		Thread.sleep(1000000L);
	}

	@Test
	public void testBatchPut() throws Exception {
		Student stu1 = new Student();
		stu1.setName("王5");
		stu1.setAge("30");
		stu1.setSex("男");
		stu1.setAddress("李5家的地址");
		Student stu2 = new Student();
		stu2.setName("王6");
		stu2.setAge("30");
		stu2.setSex("男");
		stu2.setAddress("李6家的地址");
		Student stu3 = new Student();
		stu3.setName("王7");
		stu3.setAge("30");
		stu3.setSex("男");
		stu3.setAddress("李7家的地址");
		Student stu4 = new Student();
		stu4.setName("8");
		stu4.setAge("30");
		stu4.setSex("男");
		stu4.setAddress("李8家的地址");

		List<Student> list = Arrays.asList(stu1, stu2, stu3, stu4);

		hBaseTemplate.put(list, (o, e) -> {
			if (o != null) {
				System.out.println("==== o ======"+o);
				return false;
			}
			if (e != null) {
				System.out.println("=============================");
				System.out.println(e.getClass());
				System.out.println("=============================");
				e.printStackTrace();
			}
			return null;
		});

		Thread.sleep(1000000);
	}

	@Test
	public void testPut() throws Exception {

		Student stu = new Student();
		stu.setName("李四22222");
		stu.setAge("30");
		stu.setSex("男");
		stu.setAddress("李四家的地址");

//		Student zhangsang = template.get(Student.class, "zhangsang", );

		hBaseTemplate.put(stu, (o, e) -> {
			if (o != null) {
				System.out.println("==== o ======"+o);
				return o;
			}
			if (e != null) {
				System.out.println("==============================");
				System.out.println(e.getClass());
				System.out.println("==============================");
				e.printStackTrace();
			}
			return null;
		});

		Student student = hBaseTemplate.get(Student.class, stu.getName(), (student1, e) -> {

			if (student1 != null) {
					System.out.println("|||||||||||||||"+JSON.toJSONString(student1));
			}

			if (e!=null) {
				e.printStackTrace();
			}
			return null;
		});

		System.out.println("========="+JSON.toJSONString(student));

	}

	@Test
	public void extend() {


		HBaseTemplate.CallBack<List<Student>> scanCB = new HBaseTemplate.CallBack() {

			@Override
			public List<Student> doInTable(HBaseClient client) {

				Scanner scanner = client.newScanner("SEA_TEST");
				scanner.setStartKey("0");

				ArrayList<ArrayList<KeyValue>> rows;

				List<Student> stus = new ArrayList<>();
				try {
					while ((rows = scanner.nextRows().joinUninterruptibly()) != null) {

                        for (ArrayList<KeyValue> row : rows) {
							stus.add(AnnotationParser.parseGetResponse(Student.class, row));
						}
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}

				return stus;
			}
		};
		List<Student> students = scanCB.doInTable(hBaseTemplate.getClient());

		System.out.println(JSON.toJSONString(students));

	}



}
