package com.zto.boot.hbase.model;

import com.zto.boot.hbase.annotation.HColumn;
import com.zto.boot.hbase.annotation.HRowKey;
import com.zto.boot.hbase.annotation.HTable;

import java.io.Serializable;

@HTable(name="SEA_TEST")
public class Student implements Serializable {

		@HRowKey
		@HColumn(name="name")
		private String name;

		@HColumn(name="age")
		private String age;

		@HColumn(name="sex")
		private String sex;

		@HColumn(name="address")
		private String address;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAge() {
			return age;
		}

		public void setAge(String age) {
			this.age = age;
		}

		public String getSex() {
			return sex;
		}

		public void setSex(String sex) {
			this.sex = sex;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}
	}